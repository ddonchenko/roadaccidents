import pandas as pd
import datetime
import numpy as np
import data_types_analyzed
import traceback
import sys
from peewee import *


#    Day (10 AM — 5:30 PM) - 1
#    Peak Time (8 AM — 10AM, 5:30 PM — 7:30 PM) - 2
#    Night (7:30 PM — 8 AM) - 3
def get_time_range_feature(accident_time):
    if accident_time.time() >= datetime.time(10, 0) and accident_time.time() <= datetime.time(17, 30):
        return 1
    elif (accident_time.time() >= datetime.time(8, 0) and accident_time.time() < datetime.time(10, 0)) \
            or (accident_time.time() > datetime.time(17, 30) and accident_time.time() <= datetime.time(19, 30)):
        return 2
    else:
        return 3


#    No precipitations  1
#    Rain               2
#    Snow               3
#    Snowstorm          4
#    Fog                5
def get_weather_conditions_feature(weather_conditions):
    if weather_conditions is None:
        return 1
    if 'Дождь' in weather_conditions:
        return 2
    elif 'Снегопад' in weather_conditions:
        return 3
    elif 'Метель' in weather_conditions:
        return 4
    elif 'Туман' in weather_conditions:
        return 5
    else:
        return 1


# No lightning  1
# Lightning     2
def get_road_lightning_feature(road_lightning):
    if road_lightning == 'В темное время суток, освещение не включено' or \
            road_lightning == 'В темное время суток, освещение отсутствует':
        return 1
    else:
        return 2


# Dry 1
# Wet 2
# Icy 3
def get_road_surface_feature(road_surface_slippery):
    if road_surface_slippery == 'Мокрое' or \
            road_surface_slippery == 'Залитое (покрытое) водой':
        return 2
    elif road_surface_slippery == 'Гололедица' or \
            road_surface_slippery == 'Со снежным накатом' or \
            road_surface_slippery == 'Заснеженное':
        return 3
    else:
        return 1


# Regulated Junction    1
# Unregulated Junction  2
# No junction           3
def get_junction_feature(object_name):
    if object_name is None:
        return 3
    if 'Нерегулируемый' in object_name:
        return 2
    elif 'перекресток' in object_name or 'перекрёсток' in object_name:
        return 1
    else:
        return 3


# DUI (driving under the influence):
# Sober - 1
# Alcohol/Drugs - 2
def get_dui_feature(concomitant_traffic_violation):
    if concomitant_traffic_violation is None:
        return 1
    if 'алкоголь' in concomitant_traffic_violation or 'наркотич' in concomitant_traffic_violation:
        return 2
    else:
        return 1


def get_traffic_violations_feature(traffic_violation):
    return data_types_analyzed.traffic_violations[traffic_violation]


# No - 1
# Yes - 2
def get_road_defect_feature(road_defect):
    if road_defect == '' or road_defect is None or road_defect == 'Не установлены':
        return 1
    else:
        return 2


# Cars                  1
# Two-wheel vehicle     2
# Other                 3
def get_vehicle_type_feature(vehicle_type):
    if vehicle_type is None:
        return 3
    if 'класс' in vehicle_type or \
            vehicle_type == 'Легковые автомобили (без типа)' or \
            vehicle_type == 'Спортивные (гоночные)' or \
            vehicle_type == 'Прочие легковые автомобили':
        return 1
    elif 'Мопеды' in vehicle_type or \
            vehicle_type == 'Мотоциклы' or \
            vehicle_type == 'Иные мототранспортные средства' or \
            vehicle_type == 'Мотороллеры' or \
            vehicle_type == 'Велосипеды' or \
            vehicle_type == 'Трициклы' or \
            vehicle_type == 'Мотовелосипеды' or \
            vehicle_type == 'Мотоколяски' or \
            vehicle_type == 'Мототранспорт (без типа)' or \
            vehicle_type == 'Персональное электрическое средство передвижения малой мощности':
        return 2
    else:
        return 3


# No - 1
# Yes - 2
def get_vehicle_defect_feature(vehicle_defect):
    if vehicle_defect == 'Технические неисправности отсутствуют' or vehicle_defect == 'Не устанавливались':
        return 1
    else:
        return 2


# Take log of the vehicle age
def get_vehicle_age_feature(year_of_manufacture, accident_datetime):
    if year_of_manufacture is None:
        return 1
    vehicle_age = accident_datetime.year - int(year_of_manufacture)
    return np.log1p(vehicle_age)


def get_safety_belt_feature(safety_belt):
    if safety_belt:
        return 1
    else:
        return 2


# not injured                       1
# injured                           2
# dead                              3
def get_accident_severity_feature3(injury):
    if injury == 'Не пострадал' or injury == '' or injury is None or \
            injury == 'Получил травмы с оказанием разовой медицинской помощи, к категории раненый не относится':
        return 1
    elif injury == 'Раненый, находящийся (находившийся)  на амбулаторном лечении, либо которому по характеру полученных травм обозначена необходимость амбулаторного лечения (вне зависимости от его фактического прохождения)'or \
            injury == 'Раненый, находящийся (находившийся) на стационарном лечении':
        return 2
    else:
        return 3


# not injured/minor injury                      0
# heavy wound/dead                              1
def get_accident_severity_feature2(injury):
    if injury == 'Не пострадал' or injury == '' or \
                    injury is None or \
            injury == 'Раненый, находящийся (находившийся)  на амбулаторном лечении, либо которому по характеру полученных травм обозначена необходимость амбулаторного лечения (вне зависимости от его фактического прохождения)' or \
            injury == 'Получил травмы с оказанием разовой медицинской помощи, к категории раненый не относится':
        return 0
    else:
        return 1


def is_vehicle_driver_side_damaged(region_name, car_name, damage_done):
    if damage_done is None:
        return 0

    right_wheel_regions = [
        'Чукотский автономный округ',
        'Амурская область',
        'Приморский край',
        'Магаданская область',
        'Республика Бурятия',
        'Еврейская автономная область',
        'Хабаровский край',
        'Республика Саха (Якутия)',
        'Сахалинская область',
        'Забайкальский край'
    ]
    japanese_cars = [
        'TOYOTA,'
        'HONDA',
        'MITSUBISHI',
        'NISSAN'
    ]

    if region_name in right_wheel_regions and car_name in japanese_cars:
        if 'Передний правый бок' in damage_done or 'Передний правый угол' in damage_done:
            return 1
    else:
        if 'Передний левый бок' in damage_done or 'Передний левый угол' in damage_done:
            return 1
    return 0


def is_vehicle_full_body_damaged(damage_done):
    if damage_done is not None and 'Полная деформация кузова' in damage_done:
        return 1
    return 0


def is_vehicle_on_fire(damage_done):
    if damage_done is not None and 'Возгорание' in damage_done:
        return 1
    return 0


def read_drivers_dataset(sql_query, con):
    drivers_dataset_raw = pd.read_sql_query(sql_query, con)
    data_rows = []
    for index, row in drivers_dataset_raw.iterrows():
        try:
            if index % 10000 == 0:
                print(index)
            accident_datetime = row['accident_datetime']

            vehicle_damage = row['damage_done']
            vehicle_full_body_damaged = is_vehicle_full_body_damaged(vehicle_damage)
            if vehicle_full_body_damaged:
                driver_side_damaged = 1
            else:
                region_name = row['region_name']
                car_name = row['vehicle_info']
                driver_side_damaged = is_vehicle_driver_side_damaged(region_name, car_name, vehicle_damage)

            data_row = {
                        'accident_time': get_time_range_feature(accident_datetime),
                        'accident_day_of_week': accident_datetime.weekday() + 1,
                        'weather_conditions': get_weather_conditions_feature(row['weather_precipitation']),
                        'road_lightning': get_road_lightning_feature(row['road_lightning']),
                        'road_surface': get_road_surface_feature(row['road_surface_slippery']),
                        'driving_under_influence': get_dui_feature(row['concomitant_traffic_violation']),
                        'traffic_violations': get_traffic_violations_feature(row['traffic_violation']),
                        'road_defect': get_road_defect_feature(row['defect_description']),
                        'vehicle_type': get_vehicle_type_feature(row['vehicle_type']),
                        'vehicle_defect': get_vehicle_defect_feature(row['technical_defects']),
                        'safety_belt': get_safety_belt_feature(row['safety_belt']),
                        #'vehicle_age': get_vehicle_age_feature(row['year_of_manufacture'], accident_datetime),
                        'driver_side_damaged':  driver_side_damaged,
                        'vehicle_full_body_damaged': vehicle_full_body_damaged,
                        'vehicle_on_fire': is_vehicle_on_fire(vehicle_damage),
                        'accident_severity': get_accident_severity_feature2(row['injury'])
                    }
            #print(data_row)
            data_rows.append(data_row)
        except Exception as e:
            print(traceback.format_exc())
            # or
            print(sys.exc_info()[0])

    #print(drivers_dataset.to_string())
    drivers_dataset = pd.DataFrame(data_rows)
    drivers_dataset.to_csv('drivers_dataset.csv')


database = MySQLDatabase('RoadAccidents', **{'password': 'password', 'user': 'root', 'unix_socket': '/var/run/mysqld/mysqld.sock'})
read_drivers_dataset("SELECT * FROM DRIVERS_DATASET_VIEW", database)