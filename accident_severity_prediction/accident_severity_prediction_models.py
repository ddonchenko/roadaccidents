import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
import xgboost as xgb
import matplotlib.pyplot as plt
import numpy as np
from time import time
from scipy.stats import randint as sp_randint
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from sklearn.metrics import accuracy_score
from sklearn.utils.fixes import signature
from sklearn.feature_selection import SelectFromModel
from numpy import sort
import itertools
import re
from graphviz import Digraph
from catboost import CatBoostClassifier

from imblearn.datasets import make_imbalance

dataset2_filename = 'drivers_dataset.csv'
dataset3_filename = 'drivers_dataset3.csv'

all_data = pd.read_csv(dataset2_filename)
X = all_data.loc[:, all_data.columns != 'accident_severity']
Y = all_data.accident_severity.values
#X, Y = make_imbalance(X, Y, sampling_strategy={1: 30000, 2: 25000, 3: 17731})  #3 classes
X, Y = make_imbalance(X, Y, sampling_strategy={0: 59000, 1: 59000})  #2 classes
print(all_data.groupby('accident_severity').count())
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.1, shuffle=True)
class_names3 = ["Not injured", "Injured", "Dead"]
class_names2 = ["No/Minor injury", "Heavy wound/Dead"]
feature_names = {
    0:      'Time',
    1:      'Day',
    2:      'Weather',
    3:      'Lightning',
    4:      'Surface',
    5:      'DUI',
    6:      'Violation',
    7:      'Road Defect',
    8:      'Vehicle Type',
    9:      'Vehicle Defect',
    10:     'Safety Belt',
    11:     'Vehicle Age',
    12:     'Driver Side Damaged',
    13:     'Full Damage',
    14:     'Fire',
    15:     'Severity'
}


_NODEPAT = re.compile(r'(\d+):\[(.+)\]')
_LEAFPAT = re.compile(r'(\d+):(leaf=.+)')
_EDGEPAT = re.compile(r'yes=(\d+),no=(\d+)')


def load_trees(dump_result):
    trees = []
    current_tree = []
    for line in open(dump_result):
        line = line.strip()
        if line.startswith('booster'):
            if len(current_tree):
                trees.append('\n'.join(current_tree))
                current_tree = []
        else:
            current_tree.append(line)
    trees.append('\n'.join(current_tree))
    return trees


def draw_tree(tree, out_path):
    graph = Digraph()
    tree = tree.split()
    for i, text in enumerate(tree):
        if text[0].isdigit():
            node = _pharse_node(graph, text)
        else:
            if i == 0:
                raise ValueError('Unable to phase given string as tree')
            _pharse_edge(graph, node, text, yes_colr='#0000FF', no_color='#FF0000')
    out_file = open(out_path, 'wb+')
    out_file.write(graph.pipe(format='png'))
    out_file.close()


def _pharse_node(graph, text):
    match = _NODEPAT.match(text)
    if match is not None:
        node = match.group(1)
        graph.node(node, label=match.group(2).replace('_', ' '))
        return node
    match = _LEAFPAT.match(text)

    if match is not None:
        node = match.group(1)
        graph.node(node, label=match.group(2).replace('_',' '), shape='box')
        return node
    raise ValueError('Unable to parse node: {0}'.format(text))


def _pharse_edge(graph, node, text, yes_colr='#0000FF', no_color='#FF0000'):
    match = _EDGEPAT.match(text)
    if match is not None:
        yes, no = match.groups()
        graph.edge(node, yes, label='yes', color=yes_colr)
        graph.edge(node, no, label='no', color=no_color)
        return
    raise ValueError('Unable to pharse edege: {0}'.format(text))


def svc_classifier():
    svc = SVC(verbose = True)
    svc.fit(X_train, Y_train)
    Y_pred = svc.predict(X_test)
    print('SVC Training set score: ' + str(svc.score(X_train, Y_train)))
    print('SVC Test set score: ' + str(svc.score(X_test, Y_test)))


def stochastic_gradient_descent_classifier():
    # Stochastic Gradient Descent
    sgd = SGDClassifier(verbose = True)
    sgd.fit(X_train, Y_train)
    Y_pred = sgd.predict(X_test)
    print('Stochastic Gradient Descent Training set score: ' + str(sgd.score(X_train, Y_train)))
    print('Stochastic Gradient Descent Test set score: ' + str(sgd.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


def knn_classifier():
    knn = KNeighborsClassifier(n_neighbors=15)
    knn.fit(X_train, Y_train)
    Y_pred = knn.predict(X_test)
    print('KNN Training set score: ' + str(knn.score(X_train, Y_train)))
    print('KNN Test set score: ' + str(knn.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


def logistic_regression():
    # Logistic Regression
    logreg = LogisticRegression(verbose = True)
    logreg.fit(X_train, Y_train)
    Y_pred = logreg.predict(X_test)
    print('Logistic Regression Training set score: ' + str(logreg.score(X_train, Y_train)))
    print('Logistic Regression Test set score: ' + str(logreg.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


# Decision Tree
def decision_tree():
    decision_tree = DecisionTreeClassifier(max_depth=12)
    decision_tree.fit(X_train, Y_train)
    Y_pred = decision_tree.predict(X_test)
    print('Decision Tree Training set score: ' + str(decision_tree.score(X_train, Y_train)))
    print('Decision Tree Test set score: ' + str(decision_tree.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


def perceptron():
    perceptron = Perceptron(max_iter=1000, tol=1e-3)
    perceptron.fit(X_train, Y_train)
    Y_pred = perceptron.predict(X_test)
    print('Perceptron Training set score: ' + str(perceptron.score(X_train, Y_train)))
    print('Perceptron Test set score: ' + str(perceptron.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


def gaussian_naive_bayes():
    gaussian = GaussianNB()
    gaussian.fit(X_train, Y_train)
    Y_pred = gaussian.predict(X_test)
    print('Gaussian Naive Bayes Training set score: ' + str(gaussian.score(X_train, Y_train)))
    print('Gaussian Naive Bayes Test set score: ' + str(gaussian.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


def extra_tree():
    extra_tree_clf = ExtraTreesClassifier(n_estimators=20, max_depth=50, verbose=5, n_jobs=-1)
    extra_tree_clf.fit(X_train, Y_train)
    Y_pred = extra_tree_clf.predict(X_test)
    print('Extra Trees Training set score: ' + str(extra_tree_clf.score(X_train, Y_train)))
    print('Extra Trees Test set score: ' + str(extra_tree_clf.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


# Multi-layer Perceptron classifier
def mlp_classifier():
    mlp_classifier = MLPClassifier(verbose=True, learning_rate='adaptive')
    mlp_classifier.fit(X_train, Y_train)
    Y_pred = mlp_classifier.predict(X_test)
    print('Multi-layer Perceptron classifier Training set score: ' + str(mlp_classifier.score(X_train, Y_train)))
    print('Multi-layer Perceptron classifier Test set score: ' + str(mlp_classifier.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


def gradient_boosting_classifier():
    gb_classifier = GradientBoostingClassifier(verbose=True)
    gb_classifier.fit(X_train, Y_train)
    Y_pred = gb_classifier.predict(X_test)
    print('Gradient Boosting classifier Training set score: ' + str(gb_classifier.score(X_train, Y_train)))
    print('Gradient Boosting classifier Test set score: ' + str(gb_classifier.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


def one_vs_rest_classifier():
    ovr_classifier = OneVsRestClassifier(LogisticRegression(),n_jobs=-1)
    ovr_classifier.fit(X_train, Y_train)
    Y_pred = ovr_classifier.predict(X_test)
    print('OneVsRestClassifier Training set score: ' + str(ovr_classifier.score(X_train, Y_train)))
    print('OneVsRestClassifier Test set score: ' + str(ovr_classifier.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)


# Utility function to report best scores
def report(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                results['mean_test_score'][candidate],
                results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")


def compute_confusion_matrix(Y_pred):
    # Compute confusion matrix
    cnf_matrix = confusion_matrix(Y_test, Y_pred)
    np.set_printoptions(precision=2)

    # Plot non-normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names2,
                          title='Confusion matrix, without normalization')

    # Plot normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names2, normalize=True,
                          title='Normalized confusion matrix')

    plt.show()

    precision, recall, _ = precision_recall_curve(Y_test, Y_pred)
    print("Precision: " + str(precision) + ", Recall: " + str(recall))
    average_precision = average_precision_score(Y_test, Y_pred)

    plot_precision_recall_curve(recall, precision, average_precision)
    plt.show()


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()


def plot_precision_recall_curve(recall, precision, average_precision):
    step_kwargs = ({'step': 'post'}
                   if 'step' in signature(plt.fill_between).parameters
                   else {})
    plt.step(recall, precision, color='b', alpha=0.2,
             where='post')
    plt.fill_between(recall, precision, alpha=0.2, color='b', **step_kwargs)

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title('2-class Precision-Recall curve: AP={0:0.2f}'.format(
        average_precision))


def plot_feature_importance(classifier):
    importances = classifier.feature_importances_
    std = np.std([tree.feature_importances_ for tree in classifier.estimators_],
                 axis=0)
    indices = np.argsort(importances)[::-1]

    # Print the feature ranking
    print("Feature ranking:")

    labels = []
    for indice in indices:
        labels.append(feature_names[indice])

    for f in range(X.shape[1]):
        print("%d. feature %s (%f)" % (f + 1, labels[f], importances[indices[f]]))

    # Plot the feature importances of the forest
    plt.figure()
    plt.title("Feature importances")
    plt.bar(range(X.shape[1]), importances[indices],
            color="r", yerr=std[indices], align="center")

    plt.xticks(range(X.shape[1]), labels, rotation = 45, ha='right')
    plt.xlim([-1, X.shape[1]])
    plt.tight_layout()
    plt.show()


def random_forest_parameters_search():
    random_forest = RandomForestClassifier(n_estimators=10)

    # specify parameters and distributions to sample from
    param_dist = {"max_depth": [ 50, 75, 100, None],
                  "max_features": sp_randint(1, 12),
                  "min_samples_split": sp_randint(2, 12),
                  "bootstrap": [True, False],
                  "min_samples_leaf": sp_randint(1, 10),
                  "criterion": ["gini", "entropy"]}

    # run randomized search
    n_iter_search = 20
    random_search = RandomizedSearchCV(random_forest, param_distributions=param_dist,
                                       n_iter=n_iter_search, cv=5, n_jobs=-1, verbose=1)

    start = time()
    random_search.fit(X_train, Y_train)
    print("RandomizedSearchCV took %.2f seconds for %d candidates"
          " parameter settings." % ((time() - start), n_iter_search))
    report(random_search.cv_results_)


def random_forest():
    rfcl = RandomForestClassifier(
        n_estimators=300,
        bootstrap=False,
        criterion='entropy',
        max_depth=None,
        max_features=9,
        min_samples_leaf=3,
        min_samples_split=9,
        verbose=5,
        n_jobs=-1)
    rfcl.fit(X_train, Y_train)
    Y_pred = rfcl.predict(X_test)
    print('Random Forest Training set score: ' + str(rfcl.score(X_train, Y_train)))
    print('Random Forest Test set score: ' + str(rfcl.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)
    plot_feature_importance(rfcl)


def xgb_classifier():
    xgb_clf = xgb.XGBClassifier(
        max_depth=9, min_child_weight=1, gamma=0.4, colsample_bytree=0.9, subsample=0.8, reg_alpha=0.01,
        n_jobs=-1,
        feature_names=list(feature_names.values()))
    xgb_clf.fit(X_train, Y_train)
    Y_pred = xgb_clf.predict(X_test)
    print('XGradient Boosting classifier Training set score: ' + str(xgb_clf.score(X_train, Y_train)))
    print('XGradient Boosting classifier Test set score: ' + str(xgb_clf.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)
    mapper = {'f{0}'.format(i): v for i, v in enumerate(list(feature_names.values()))}
    mapped = {mapper[k]: v for k, v in xgb_clf.get_booster().get_fscore().items()}
    xgb.plot_importance(mapped)
    plt.show()
    xgb_clf.get_booster().dump_model("xgbmodel.txt", fmap="feature_map.txt")
    trees = load_trees("xgbmodel.txt")
    #for i, tree in enumerate(trees):
    #    draw_tree(trees[i], "tree" + str(i) + ".png")


def xgb_classifier_with_feature_selection():
    # fit model on all training data
    model = xgb.XGBClassifier(max_depth=9,
                              min_child_weight=3,
                              subsample=0.6,
                              colsample_bytree=0.9,
                              scale_pos_weight=1,
                              reg_alpha=0.1,
                              n_jobs=-1)
    model.fit(X_train, Y_train)
    # make predictions for test data and evaluate
    y_pred = model.predict(X_test)
    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(Y_test, predictions)
    print("Accuracy: %.2f%%" % (accuracy * 100.0))
    # Fit model using each importance as a threshold
    thresholds = sort(model.feature_importances_)
    for thresh in thresholds:
        # select features using threshold
        selection = SelectFromModel(model, threshold=thresh, prefit=True)
        select_X_train = selection.transform(X_train)
        # train model
        selection_model = xgb.XGBClassifier(max_depth=9,
                                            min_child_weight=3,
                                            subsample=0.6,
                                            colsample_bytree=0.9,
                                            scale_pos_weight=1,
                                            reg_alpha=0.1,
                                            n_jobs=-1)
        selection_model.fit(select_X_train, Y_train)
        # eval model
        select_X_test = selection.transform(X_test)
        y_pred = selection_model.predict(select_X_test)
        predictions = [round(value) for value in y_pred]
        accuracy = accuracy_score(Y_test, predictions)
        print("Thresh=%.3f, n=%d, Accuracy: %.2f%%" % (thresh, select_X_train.shape[1], accuracy * 100.0))


def xgb_parameters_search():
    xgb_clf = xgb.XGBClassifier(n_jobs=-1, max_depth=9, min_child_weight=1, gamma=0.4, colsample_bytree=0.9, subsample=0.8, reg_alpha=0.01)

    # specify parameters and distributions to sample from
    param_grid = {
        #'gamma': [i / 10.0 for i in range(0, 5)],
        #'eta': [i / 10.0 for i in range(0, 10, 2)],
        #'max_depth':range(8, 10, 1),                                 #9
        #'min_child_weight':range(1,5,1),                           #3
        #'subsample': [i / 10.0 for i in range(6, 10)],             #0.6
        #'colsample_bytree': [i / 10.0 for i in range(6, 10)],       #0.9
        #'reg_alpha': [1e-5, 1e-2, 0.1, 1, 100]                      #0.1
    }

    # run randomized search
    grid_search = GridSearchCV(xgb_clf, param_grid=param_grid,  cv=5, n_jobs=-1, verbose=1)

    start = time()
    grid_search.fit(X_train, Y_train)
    print("GrSearchCVid took %.2f seconds"
          " parameter settings." % ((time() - start)))
    report(grid_search.cv_results_)


def catboost_classifier():
    catboost_clf = CatBoostClassifier(task_type='CPU')
    start = time()
    catboost_clf.fit(X_train, Y_train)
    Y_pred = catboost_clf.predict(X_test)
    print("CatBoost Training took %.2f seconds"
          " parameter settings." % ((time() - start)))
    print('Catboost Training set score: ' + str(catboost_clf.score(X_train, Y_train)))
    print('Catboost Test set score: ' + str(catboost_clf.score(X_test, Y_test)))
    compute_confusion_matrix(Y_pred)
    #plot_feature_importance(rfcl)


def catboost_parameters_search():
    catboost_clf = CatBoostClassifier()
    params = {'depth': [3, 1, 2, 6, 4, 5, 7, 8, 9, 10],
              'iterations': [250, 100, 500, 1000],
              'learning_rate': [0.03, 0.001, 0.01, 0.1, 0.2, 0.3],
              'l2_leaf_reg': [3, 1, 5, 10, 100],
              'border_count': [32, 5, 10, 20, 50, 100, 200],
              'ctr_border_count': [50, 5, 10, 20, 100, 200],}
    random_search = RandomizedSearchCV(catboost_clf, param_distributions=params, n_jobs=-1, verbose=1)
    start = time()
    random_search.fit(X_train, Y_train)
    print("RandomSearch took %.2f seconds"
          " parameter settings." % ((time() - start)))
    report(random_search.cv_results_)


#svc_classifier()
# stochastic_gradient_descent_classifier()
#knn_classifier()
#logistic_regression()
#decision_tree()
# perceptron()
#random_forest()
#extra_tree()
#gaussian_naive_bayes()
#mlp_classifier()
#gradient_boosting_classifier()
#xgb_classifier()
#xgb_classifier_with_feature_selection()
#one_vs_rest_classifier()

#random_forest_parameters_search()
#xgb_parameters_search()
catboost_classifier()
#catboost_parameters_search()