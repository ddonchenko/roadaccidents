from bs4 import BeautifulSoup
from selenium import webdriver
import time
import csv
from fuzzywuzzy import fuzz


# Получить с помощью selenium исходный код страницы
def get_page_source(url):
    options = webdriver.ChromeOptions()
    #options.add_argument('--headless')
    options.add_argument('--log-level=3')
    browser = webdriver.Chrome(options=options)
    browser.get(url)
    print('Please wait. Data is loading...')
    time.sleep(10)
    return browser.page_source


# Возможность записи всех спарсенных машин в .csv
def write_base(data):
    with open('cars_base.csv', 'a', encoding='utf8') as f:
        writer = csv.writer(f)
        writer.writerow((data['name'],
                         data['stars'],
                         data['adult-occupant'],
                         data['child-occupant'],
                         data['pedestrian'],
                         data['safety-assist']))


# поиск соответствия возвращение значения релевантности
def match_query(query, data, min_score=0):
    max_score = -1
    score = fuzz.ratio(query, data)
    if (score > min_score) & (score > max_score):
        max_score = score

    return max_score


# парсинг урла, используем суп
def parse_site(url):
    cars_base = []
    page_source = get_page_source(url)
    soup = BeautifulSoup(page_source, 'lxml')
    cars = soup.find_all('div', class_='rating-table-row')
    for car in cars:
        # name
        try:
            div = car.find('span', class_='name')
            name = div.text.strip()
        except:
            name = ''
        # stars
        try:
            div = car.find('span', class_='stars').find('img')
            stars = div.get('alt').strip()
        except:
            stars = 'No stars'
        # adult-occupant
        try:
            div = car.find('div', class_='adult-occupant').find('span')
            if div is not None:
                adult_occupant = div.text.strip()
            else:
                div = car.find(
                    'div', class_='adult-occupant').find_all('img')[1]
                adult_occupant = div.get('alt').strip() + ' stars'
        except:
            adult_occupant = 'No adult-occupant'
        # child-occupant
        try:
            div = car.find('div', class_='child-occupant').find('span')
            if div is not None:
                child_occupant = div.text.strip()
            else:
                div = car.find(
                    'div', class_='child-occupant').find_all('img')[1]
                child_occupant = div.get('alt').strip() + ' stars'
        except:
            child_occupant = 'No child-occupant'
        # pedestrian
        try:
            div = car.find('div', class_='pedestrian').find('span')
            if div is not None:
                pedestrian = div.text.strip()
            else:
                div = car.find(
                    'div', class_='pedestrian').find_all('img')[1]
                pedestrian = div.get('alt').strip() + ' stars'
        except:
            pedestrian = 'No pedestrian'
        # safety-assist
        try:
            div = car.find('div', class_='safety-assist').find('span')
            safety_assist = div.text.strip()
        except:
            safety_assist = 'No safety-assist'
        if name == '':
            continue
        else:
            cars_base.append({'name': name,
                              'stars': stars,
                              'adult-occupant': adult_occupant,
                              'child-occupant': child_occupant,
                              'pedestrian': pedestrian,
                              'safety-assist': safety_assist})
    return cars_base


# создаем списоок на оснве найденных согласно запроса машин и очков релевантности
def find_fuzzy_cars(input_query, input_score, cars):
    car_list = []
    for car in cars:
        match = match_query(input_query, car['name'], min_score=input_score)
        if match > 0:
            dict_ = {}
            dict_.update({'name': car['name'],
                          'stars': car['stars'],
                          'adult-occupant': car['adult-occupant'],
                          'child-occupant': car['child-occupant'],
                          'pedestrian': car['pedestrian'],
                          'safety-assist': car['safety-assist'],
                          'score': match})
            car_list.append(dict_)

    sorted_list = sorted(car_list, key=lambda k: k['score'], reverse=True)
    return sorted_list


# вывод на печать списка машин
def print_cars(car_list):
    for l in car_list:
        print(l['name'], l['stars'], l['adult-occupant'], l['child-occupant'], l['pedestrian'],l['safety-assist'],l['score'], sep=' | ')


def main():
    url_alfa_ = 'https://www.euroncap.com/en/ratings-rewards/latest-safety-ratings/?selectedMake=7276&selectedMakeName=Alfa%20Romeo&selectedModel=&selectedModelName=Model&nodeId=1598&allProtocols=true&selectedStar=&includeFullSafetyPackage=true&includeStandardSafetyPackage=true&selectedProtocols=34803,30636,26061,24370,1472,5910,5931,-1,14999&selectedClasses=1202,1199,1201,1196,1205,1203,1198,1179,1197,1204,1180,34736&allClasses=true&allDriverAssistanceTechnologies=false&selectedDriverAssistanceTechnologies=&thirdRowFitment=false'
    url_all = 'https://www.euroncap.com/en/ratings-rewards/latest-safety-ratings/#?selectedMake=0&selectedMakeName=Select%20a%20make&selectedModel=0&selectedStar=&includeFullSafetyPackage=true&includeStandardSafetyPackage=true&selectedModelName=All&selectedProtocols=34803,30636,26061,24370,1472,5910,5931,-1,14999&selectedClasses=1202,1199,1201,1196,1205,1203,1198,1179,1197,1204,1180,34736&allClasses=true&allProtocols=true&allDriverAssistanceTechnologies=false&selectedDriverAssistanceTechnologies=&thirdRowFitment=false'
    cars = []
    loop = True
    while loop:
        print('														')
        print(' 1 - Parse site')
        print(' 2 - Search for cars')
        print(' 3 - Write cars to .csv')
        print(' 0 - Exit')
        print('===================================================')
        response = input('Enter a selection -> ')
        if response == '1': # Show all recipes
            cars = parse_site(url_all)
            if len(cars) > 0:
                print('Success! ' + str(len(cars)) + ' cars found')
            else:
                print('Error! Can''t load the page. Please try parse again!')
        elif response == '2': # Search for a recipe
            if len(cars) > 0:
                input_query = input('Enter query: ')
                input_score = int(input('Enter score: '))
                result = find_fuzzy_cars(input_query, input_score, cars)
                print_cars(result)
            else:
                print('Error! Base is empty. Try to parse site!')
        elif response == '3': # Show a single recipe
            if len(cars) > 0:
                for c in cars:
                    write_base(c)
            else:
                print('Error! Base is empty. Try to parse site!')
        elif response == '0': # Exit the program
            print('Bye')
            loop = False
        else:
            print('Try again.')


if __name__ == '__main__':
    main()