import os
import xml.etree.cElementTree as ET
from datetime import datetime

from data_collection.data_types_analyzed import *
from get_osm_data import *
from road_accidents_db_model import *
from xml_tags import *


def get_float_or_zero(number_str):
    try:
        return float(number_str)
    except Exception:
        return 0


def get_int_or_zero(number_str):
    try:
        return int(number_str)
    except Exception:
        return 0


def parse_road_accident(ra_xml, ra_info_xml, latitude, longitude):

    return RoadAccident.insert({
        RoadAccident.accident_type: ra_xml.find(road_accident_type_tag).text,
        RoadAccident.district : ra_xml.find(district_tag).text,
        # datetime format: dd.MM.yyyy hh:mm
        RoadAccident.accident_datetime: datetime.strptime(ra_xml.find(date_tag).text + ' ' + ra_xml.find(time_tag).text,
                                                            '%d.%m.%Y %H:%M'),
        RoadAccident.latitude: latitude,
        RoadAccident.longitude: longitude,
        RoadAccident.consequence: ra_info_xml.find(road_accident_consequences_tag).text,
        RoadAccident.factor: ra_info_xml.find(factor_tag).text,
        RoadAccident.road_lightning : ra_info_xml.find(lightning_tag).text,
        RoadAccident.road_surface_slippery: ra_info_xml.find(road_surface_slippery_tag).text,
        RoadAccident.street_type: ra_info_xml.find(street_type_tag).text,
        RoadAccident.weather_precipitation: ra_info_xml.find(weather_precipitation_tag).text
    }).execute()


def parse_vehicle(vehicle_xml, road_accident_id):

    vehicle_mark = ""
    vehicle_model = ""

    if vehicle_xml.find(vehicle_mark_tag).text is not None:
        vehicle_mark = vehicle_xml.find(vehicle_mark_tag).text
    if vehicle_xml.find(vehicle_model_tag).text is not None:
        vehicle_model = vehicle_xml.find(vehicle_model_tag).text

    year_of_manufacture = get_int_or_zero(vehicle_xml.find(vehicle_year_of_manufacture_tag).text)

    return Vehicle.insert({
        Vehicle.road_accident: road_accident_id,
        Vehicle.technical_defects: vehicle_xml.find(vehicle_defect_tag).text,
        Vehicle.vehicle_info: (vehicle_mark + ' ' + vehicle_model),
        Vehicle.year_of_manufacture: year_of_manufacture
    }).execute()


def parse_participants(parent_xml, participant_tag, road_accident_id, vehicle_id = None):

    participants = []

    for participant_xml in parent_xml.findall(participant_tag):

        alco_level = get_int_or_zero(participant_xml.find(alco_level_tag).text)
        safety_belt = False
        if participant_xml.find(safety_belt_tag) is not None and participant_xml.find(safety_belt_tag).text == 'Да':
            safety_belt = True

        participants.append({
            Participant.road_accident: road_accident_id,
            Participant.vehicle_id: vehicle_id,
            Participant.alco_level: alco_level,
            Participant.traffic_violation: participant_xml.find(traffic_violation_tag).text,
            Participant.concomitant_traffic_violation: participant_xml.find(concomitant_traffic_violation_tag).text,
            Participant.injury: participant_xml.find(injury_tag).text,
            Participant.participant_role: participant_xml.find(participant_role_tag).text,
            Participant.safety_belt: safety_belt,
            Participant.sex: participant_xml.find(sex_tag).text,
        })

    if len(participants) > 0:
        Participant.insert_many(participants).execute()


def insert_road_objects(parent_tag, object_tag, road_accident_id):

    road_objects = []

    for road_object_xml in parent_tag.findall(object_tag):
        road_objects.append({
            RoadObject.road_accident: road_accident_id,
            RoadObject.object_name: road_object_xml.text
        })

    if len(road_objects) > 0:
        RoadObject.insert_many(road_objects).execute()


def insert_road_defects(parent_tag, defect_tag, road_accident_id):

    road_defects = []

    for road_defect_xml in parent_tag.findall(defect_tag):
        road_defects.append({
            RoadDefect.road_accident: road_accident_id,
            RoadDefect.defect_description: road_defect_xml.text
        })

    if len(road_defects) > 0:
        RoadDefect.insert_many(road_defects).execute()


def get_ways_from_osm(road_accident_id, latitude, longitude):
    osm_ways = get_ways_by_coordinates(latitude, longitude)
    db_ways = []

    if osm_ways is not None:
        for osm_way in osm_ways:
            osm_id = osm_way.id
            name = osm_way.tags.get("name", "")
            highway = osm_way.tags.get("highway", "")
            maxspeed = osm_way.tags.get("maxspeed", "")
            lanes = osm_way.tags.get("lanes", "")
            lit = osm_way.tags.get("lit", "")
            crossing = osm_way.tags.get("crossing", "")

            oneway = False
            if osm_way.tags.get("oneway", "") == "yes":
                oneway = True

            if maxspeed in osm_maxspeed_ru_types:
                maxspeed_value = osm_maxspeed_ru_types[maxspeed]
            else:
                maxspeed_value = get_int_or_zero(maxspeed)

            db_ways.append({
                Way.osm_id: osm_id,
                Way.road_accident: road_accident_id,
                Way.name: name,
                Way.maxspeed: maxspeed_value,
                Way.highway: highway,
                Way.lanes: get_int_or_zero(lanes),
                Way.oneway: oneway,
                Way.lit: lit,
                Way.crossing: crossing
            })

    if len(db_ways) > 0:
        Way.insert_many(db_ways).execute()


# Parse xml files and insert data_raw in database

xml_files_folder = 'data_raw/moscow_2015_2018'
xml_files = os.listdir(xml_files_folder)

files_count = len(xml_files)
current_file = 0

for xml_file in xml_files:
    tree = ET.parse(xml_files_folder + '/' + xml_file)
    root = tree.getroot()
    current_file += 1
    print(xml_file + ", progress {0}/{1}".format(str(current_file), str(files_count)))

    with database.atomic():
        for ra_xml in root.findall(road_accident_tag):
            try:
                ra_info_xml = ra_xml.find(road_accident_info_tag)
                latitude = get_float_or_zero(ra_info_xml.find(latitude_tag).text)
                longitude = get_float_or_zero(ra_info_xml.find(longitude_tag).text)

                road_accident_id = parse_road_accident(ra_xml, ra_info_xml, latitude, longitude)

                get_ways_from_osm(road_accident_id, latitude, longitude)

                for vehicle_xml in ra_info_xml.findall(vehicle_tag):
                    try:
                        vehicle_id = parse_vehicle(vehicle_xml, road_accident_id)
                        parse_participants(vehicle_xml, participant_tag, road_accident_id, vehicle_id)
                    except Exception as ex:
                        print(ex)

                try:
                    parse_participants(ra_info_xml, participant_tag_2, road_accident_id)
                except Exception as ex:
                    print(ex)

                try:
                    insert_road_objects(ra_info_xml, objects1_tag, road_accident_id)
                except Exception as ex:
                    print(ex)

                try:
                    insert_road_objects(ra_info_xml, objects2_tag, road_accident_id)
                except Exception as ex:
                    print(ex)

                try:
                    insert_road_defects(ra_info_xml, road_defect_tag, road_accident_id)
                except Exception as ex:
                    print(ex)

            except Exception as ex:
                print(ex)