import os
import ujson
from datetime import datetime

from data_collection.data_types_analyzed import *
from get_osm_data import *
from json_tags import *
from road_accidents_db_model import *


def get_float_or_zero(number_str):
    try:
        return float(number_str)
    except Exception:
        return 0


def get_int_or_zero(number_str):
    try:
        return int(number_str)
    except Exception:
        return 0


def parse_road_accident(ra_json, ra_info_json, latitude, longitude, region_name):

    return RoadAccident.insert({
        RoadAccident.accident_type: ra_json.get(road_accident_type_tag),
        RoadAccident.region_name: region_name,
        RoadAccident.district : ra_json.get(district_tag),
        # datetime format: dd.MM.yyyy hh:mm
        RoadAccident.accident_datetime: datetime.strptime(ra_json.get(date_tag) + ' ' + ra_json.get(time_tag),
                                                            '%d.%m.%Y %H:%M'),
        RoadAccident.latitude: latitude,
        RoadAccident.longitude: longitude,
        RoadAccident.consequence: ra_info_json.get(road_accident_consequences_tag),
        RoadAccident.factor: ra_info_json.get(factor_tag),
        RoadAccident.road_lightning : ra_info_json.get(lightning_tag),
        RoadAccident.road_surface_slippery: ra_info_json.get(road_surface_slippery_tag),
        RoadAccident.street_type: ra_info_json.get(street_type_tag),
        RoadAccident.weather_precipitation: ra_info_json.get(weather_precipitation_tag)
    }).execute()


def parse_vehicle(vehicle_json, road_accident_id):

    vehicle_mark = ""
    vehicle_model = ""

    if vehicle_json.get(vehicle_mark_tag) is not None:
        vehicle_mark = vehicle_json.get(vehicle_mark_tag)
    if vehicle_json.get(vehicle_model_tag) is not None:
        vehicle_model = vehicle_json.get(vehicle_model_tag)

    year_of_manufacture = get_int_or_zero(vehicle_json.get(vehicle_year_of_manufacture_tag))

    return Vehicle.insert({
        Vehicle.road_accident: road_accident_id,
        Vehicle.technical_defects: vehicle_json.get(vehicle_defect_tag),
        Vehicle.vehicle_info: (vehicle_mark + ' ' + vehicle_model),
        Vehicle.year_of_manufacture: year_of_manufacture,
        Vehicle.vehicle_type: vehicle_json.get(vehicle_type_tag),
        Vehicle.damage_done: vehicle_json.get(vehicle_damage_tag)
    }).execute()


def parse_participants(parent_json, participant_tag, road_accident_id, vehicle_id = None):

    participants = []

    for participant_json in parent_json.get(participant_tag):

        alco_level = get_int_or_zero(participant_json.get(alco_level_tag))
        safety_belt = False
        if participant_json.get(safety_belt_tag) is not None and participant_json.get(safety_belt_tag) == 'Да':
            safety_belt = True

        participants.append({
            Participant.road_accident: road_accident_id,
            Participant.vehicle_id: vehicle_id,
            Participant.alco_level: alco_level,
            Participant.traffic_violation: participant_json.get(traffic_violation_tag)[0],
            Participant.concomitant_traffic_violation: participant_json.get(concomitant_traffic_violation_tag)[0],
            Participant.injury: participant_json.get(injury_tag),
            Participant.participant_role: participant_json.get(participant_role_tag),
            Participant.safety_belt: safety_belt,
            Participant.sex: participant_json.get(sex_tag),
        })

    if len(participants) > 0:
        Participant.insert_many(participants).execute()


def insert_road_objects(parent_tag, object_tag, road_accident_id):

    road_objects = []

    for road_object_json in parent_tag.get(object_tag):
        road_objects.append({
            RoadObject.road_accident: road_accident_id,
            RoadObject.object_name: road_object_json
        })

    if len(road_objects) > 0:
        RoadObject.insert_many(road_objects).execute()


def insert_road_defects(parent_tag, defect_tag, road_accident_id):

    road_defects = []

    for road_defect_json in parent_tag.get(defect_tag):
        road_defects.append({
            RoadDefect.road_accident: road_accident_id,
            RoadDefect.defect_description: road_defect_json
        })

    if len(road_defects) > 0:
        RoadDefect.insert_many(road_defects).execute()


def get_ways_from_osm(road_accident_id, latitude, longitude):
    osm_ways = get_ways_by_coordinates(latitude, longitude)
    db_ways = []

    if osm_ways is not None:
        for osm_way in osm_ways:
            osm_id = osm_way.id
            name = osm_way.tags.get("name", "")
            highway = osm_way.tags.get("highway", "")
            maxspeed = osm_way.tags.get("maxspeed", "")
            lanes = osm_way.tags.get("lanes", "")
            lit = osm_way.tags.get("lit", "")
            crossing = osm_way.tags.get("crossing", "")

            oneway = False
            if osm_way.tags.get("oneway", "") == "yes":
                oneway = True

            if maxspeed in osm_maxspeed_ru_types:
                maxspeed_value = osm_maxspeed_ru_types[maxspeed]
            else:
                maxspeed_value = get_int_or_zero(maxspeed)

            db_ways.append({
                Way.osm_id: osm_id,
                Way.road_accident: road_accident_id,
                Way.name: name,
                Way.maxspeed: maxspeed_value,
                Way.highway: highway,
                Way.lanes: get_int_or_zero(lanes),
                Way.oneway: oneway,
                Way.lit: lit,
                Way.crossing: crossing
            })

    if len(db_ways) > 0:
        Way.insert_many(db_ways).execute()


# Parse xml files and insert data_raw in database
def parse_folder(folder_path):
    print("Parsing " + folder_path + " folder")

    files = os.listdir(folder_path)
    files_count = len(files)
    current_file = 0
    gen_expr = (file for file in files if file.endswith('.json'))

    for file in gen_expr:
        with open(os.path.join(folder_path, file), 'r') as ujson_file:
            ujson_str = ujson_file.read()
            current_json = ujson.loads(ujson.loads(ujson_str).get("data"))
            region_name = current_json.get(region_name_tag)
            current_file += 1
            print(file + ", progress {0}/{1}".format(str(current_file), str(files_count)))

            with database.atomic():
                for ra_json in current_json.get(road_accident_tag):
                    try:
                        ra_info_json = ra_json.get(road_accident_info_tag)
                        latitude = get_float_or_zero(ra_info_json.get(latitude_tag))
                        longitude = get_float_or_zero(ra_info_json.get(longitude_tag))

                        road_accident_id = parse_road_accident(ra_json, ra_info_json, latitude, longitude, region_name)

                        # get_ways_from_osm(road_accident_id, latitude, longitude)

                        for vehicle_xml in ra_info_json.get(vehicle_tag):
                            try:
                                vehicle_id = parse_vehicle(vehicle_xml, road_accident_id)
                                parse_participants(vehicle_xml, participant_tag, road_accident_id, vehicle_id)
                            except Exception as ex:
                                print(ex)

                        try:
                            parse_participants(ra_info_json, participant_tag_2, road_accident_id)
                        except Exception as ex:
                            print(ex)

                        try:
                            insert_road_objects(ra_info_json, objects1_tag, road_accident_id)
                        except Exception as ex:
                            print(ex)

                        try:
                            insert_road_objects(ra_info_json, objects2_tag, road_accident_id)
                        except Exception as ex:
                            print(ex)

                        try:
                            insert_road_defects(ra_info_json, road_defect_tag, road_accident_id)
                        except Exception as ex:
                            print(ex)

                    except Exception as ex:
                        print(ex)


root_folder = "../data_raw/json"
folders = os.listdir(root_folder)
for folder in folders:
    parse_folder(os.path.join(root_folder, folder))