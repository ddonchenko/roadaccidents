import overpy
import time

api = overpy.Overpass()


def get_ways_by_coordinates(latitude, longitude):
    retry_count = 0
    diff = 0.0001
    south = latitude - diff
    west = longitude - diff
    north = latitude + diff
    east = longitude + diff
    # South, West, North, East
    bbox_str = str(south) + "," + str(west) + "," + str(north) + "," + str(east)
    get_ways_query = """
                way({0}) ["highway"];
                (._;>;);
                out body;
                """.format(bbox_str)
    while True:
        try:
            result = api.query(get_ways_query)
            return result.ways
        except Exception as ex:
            retry_count += 1
            print("Get Ways by coordinates failed. Latitude: {0}, Longitude: {1}, retry #{2}".format(str(latitude),
                                                                                                     str(longitude),
                                                                                                     str(retry_count)))
            print(ex)
        except overpy.exception.OverpassTooManyRequests as ex2:
            retry_count += 1
            print("Overpass too many requests exception occured, retry #{0}. Wait for 30 sec and then try again.".format(str(retry_count)))
            print(ex2)
            time.sleep(30)

        if retry_count == 3:
            return None
    return None


# for way in result.ways:
#     print("Name: %s" % way.tags.get("name", "n/a"))
#     print("  Highway: %s" % way.tags.get("highway", "n/a"))
#     print("  Maxspeed: %s" % way.tags.get("maxspeed", "n/a"))
#     print("  Lanes: %s" % way.tags.get("lanes", "n/a"))
#     print("  Oneway: %s" % way.tags.get("oneway", "n/a"))
#     print("  Nodes:")
#     for node in way.nodes:
#         print("    Lat: %f, Lon: %f" % (node.lat, node.lon))

# get_ways_by_coordinates(55.775000,37.683400)