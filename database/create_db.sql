CREATE DATABASE `RoadAccidents` DEFAULT CHARACTER SET utf8;

use `RoadAccidents`;

CREATE TABLE `RoadAccident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accident_type` varchar(250) NOT NULL,
  `accident_datetime` datetime NOT NULL,
  `latitude` decimal(10,6) NOT NULL,
  `longitude` decimal(10,6) NOT NULL,
  `street_type` varchar(250) DEFAULT NULL,
  `weather_precipitation` varchar(250) DEFAULT NULL,
  `road_lightning` varchar(250) DEFAULT NULL,
  `road_surface_slippery` varchar(250) DEFAULT NULL,
  `factor` varchar(500) DEFAULT NULL,
  `consequence` varchar(500) DEFAULT NULL,
  `region_name` varchar(250) DEFAULT NULL,
  `district` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Vehicle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `road_accident_id` int(11) NOT NULL,
  `vehicle_info` varchar(250) NOT NULL,
  `vehicle_type` varchar(250) NOT NULL,
  `year_of_manufacture` int(11) DEFAULT NULL,
  `technical_defects` varchar(500) DEFAULT NULL,
  `damage_done` varchar(500) DEFAULT NULL,
  `adult_occupant_safety` decimal(2, 2) DEFAULT NULL,
  `child_occupant_safety` decimal(2, 2) DEFAULT NULL,
  `pedestrian_safety` decimal(2, 2) DEFAULT NULL,
  `safety_assist` decimal(2, 2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Vehicle_fk0` (`road_accident_id`),
  CONSTRAINT `Vehicle_fk0` FOREIGN KEY (`road_accident_id`) REFERENCES `RoadAccident` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `road_accident_id` int(11) NOT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `participant_role` varchar(250) NOT NULL,
  `sex` varchar(50) DEFAULT NULL,
  `alco_level` int(11) DEFAULT NULL,
  `traffic_violation` varchar(250) DEFAULT NULL,
  `concomitant_traffic_violation` varchar(500) DEFAULT NULL,
  `safety_belt` bit(1) DEFAULT NULL,
  `injury` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Participant_fk0` (`road_accident_id`),
  KEY `Participant_fk1` (`vehicle_id`),
  CONSTRAINT `Participant_fk0` FOREIGN KEY (`road_accident_id`) REFERENCES `RoadAccident` (`id`),
  CONSTRAINT `Participant_fk1` FOREIGN KEY (`vehicle_id`) REFERENCES `Vehicle` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `RoadDefect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `road_accident_id` int(11) NOT NULL,
  `defect_description` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `RoadDefect_fk0` (`road_accident_id`),
  CONSTRAINT `RoadDefect_fk0` FOREIGN KEY (`road_accident_id`) REFERENCES `RoadAccident` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `RoadObject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `road_accident_id` int(11) NOT NULL,
  `object_name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `RoadObject_fk0` (`road_accident_id`),
  CONSTRAINT `RoadObject_fk0` FOREIGN KEY (`road_accident_id`) REFERENCES `RoadAccident` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Way` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `osm_id` varchar(45) NOT NULL,
  `road_accident_id` int(11) NOT NULL,
  `highway` varchar(150) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `lanes` int(11) DEFAULT NULL,
  `maxspeed` int(11) DEFAULT NULL,
  `oneway` bit(1) DEFAULT NULL,
  `lit` varchar(45) DEFAULT NULL,
  `crossing` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Ways_fk0` (`road_accident_id`),
  CONSTRAINT `Ways_fk0` FOREIGN KEY (`road_accident_id`) REFERENCES `RoadAccident` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
