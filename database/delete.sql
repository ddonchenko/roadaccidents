use RoadAccidents;
SET SQL_SAFE_UPDATES = 0;
DELETE FROM RoadAccidents.Participant;
DELETE FROM RoadAccidents.Vehicle;
DELETE FROM RoadAccidents.RoadDefect;
DELETE FROM RoadAccidents.RoadObject;
DELETE FROM RoadAccidents.RoadAccident;
DELETE FROM RoadAccidents.Way;
