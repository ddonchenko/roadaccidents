use RoadAccidents;

CREATE VIEW DRIVERS_DATASET_VIEW AS
SELECT
ra.id as road_accident_id, ra.accident_datetime, ra.accident_type, ra.weather_precipitation, ra.road_lightning, ra.road_surface_slippery, ra.region_name,
ro.object_name,
p.id as participant_id, p.alco_level, p.traffic_violation, p.concomitant_traffic_violation, p.injury, p.safety_belt,
rd.id as road_defect_id, rd.defect_description,
v.id as vehicle_id, v.vehicle_type, v.technical_defects, v.year_of_manufacture, v.damage_done
FROM RoadAccidents.RoadAccident ra
left join
	(
    select distinct object_name, road_accident_id from RoadObject
	where
	object_name like '%перекрёсток%' or
	object_name like '%перекресток%' or
	object_name like '%пересечение%') ro on
ro.road_accident_id = ra.id
join
	(
    select distinct id, road_accident_id, vehicle_id, participant_role, alco_level, traffic_violation, concomitant_traffic_violation, injury, safety_belt from Participant
    where participant_role = 'Водитель') p on
p.road_accident_id = ra.id
join
	(
    select id, road_accident_id, defect_description from RoadDefect
    where defect_description <> 'Не установлены'
    ) rd on
rd.road_accident_id = ra.id
left join Vehicle v on
v.road_accident_id = ra.id and p.vehicle_id = v.id and
accident_type <> 'Наезд на пешехода' and accident_type <> 'Наезд на велосипедиста'
#LIMIT 1000
