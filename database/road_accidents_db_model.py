from peewee import *

database = MySQLDatabase('RoadAccidents', **{'password': 'password', 'user': 'root', 'unix_socket': '/var/run/mysqld/mysqld.sock'})


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = database


class RoadAccident(BaseModel):
    accident_datetime = DateTimeField()
    accident_type = CharField()
    consequence = CharField(null=True)
    factor = CharField(null=True)
    latitude = FloatField()
    longitude = FloatField()
    road_lightning = CharField(null=True)
    road_surface_slippery = CharField(null=True)
    street_type = CharField(null=True)
    weather_precipitation = CharField(null=True)
    region_name = CharField(null=True)
    district = CharField(null=True)

    class Meta:
        table_name = 'RoadAccident'
        db_table = 'RoadAccident'


class Vehicle(BaseModel):
    road_accident = ForeignKeyField(db_column='road_accident_id', model=RoadAccident, to_field='id')
    vehicle_type = CharField()
    vehicle_info = CharField()
    year_of_manufacture = IntegerField(null=True)
    technical_defects = CharField(null=True)
    damage_done = CharField(null=True)
    adult_occupant_safety = FloatField(null=True)
    child_occupant_safety = FloatField(null=True)
    pedestrian_safety = FloatField(null=True)
    safety_assist = FloatField(null=True)

    class Meta:
        table_name = 'Vehicle'
        db_table = 'Vehicle'


class Participant(BaseModel):
    alco_level = IntegerField(null=True)
    concomitant_traffic_violation = CharField(null=True)
    injury = CharField(null=True)
    participant_role = CharField()
    road_accident = ForeignKeyField(db_column='road_accident_id', model=RoadAccident, to_field='id')
    safety_belt = BooleanField(null=True)  # bit
    sex = CharField(null=True)
    traffic_violation = CharField(null=True)
    vehicle = ForeignKeyField(db_column='vehicle_id', null=True, model=Vehicle, to_field='id')

    class Meta:
        table_name = 'Participant'
        db_table = 'Participant'


class RoadDefect(BaseModel):
    defect_description = CharField()
    road_accident = ForeignKeyField(db_column='road_accident_id', model=RoadAccident, to_field='id')

    class Meta:
        table_name = 'RoadDefect'
        db_table = 'RoadDefect'


class Way(BaseModel):
    osm_id = CharField()
    road_accident = ForeignKeyField(db_column='road_accident_id', model=RoadAccident, to_field='id')
    highway = CharField(null=True)
    name = CharField(null=True)
    lanes = IntegerField(null=True)
    maxspeed = IntegerField(null=True)
    oneway = BooleanField(null=True)  # bit
    lit = CharField(null=True)
    crossing = CharField(null=True)

    class Meta:
        table_name = 'Way'
        db_table = 'Way'


class RoadObject(BaseModel):
    object_name = CharField()
    road_accident = ForeignKeyField(db_column='road_accident_id', model=RoadAccident, to_field='id')

    class Meta:
        table_name = 'RoadObject'
        db_table = 'RoadObject'
