function updateTooltip(object) {
    var props = object.object.extraProps;
    accident_info =
        "Тип: " + props.accident_type +
        "\nОсвещение: " + props.road_lightning +
        "\nПогода: " + props.weather_precipitation +
        "\nДорожное покрытие: " + props.road_surface_slippery +
        "\nТип улицы: " + props.street_type +
        "\nДоп. фактор: " + props.factor +
        "\nПоследствия: " + props.consequence;

    return accident_info;
}